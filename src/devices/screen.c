#include <stdlib.h>
#include <string.h>

#include "../uxn.h"
#include "screen.h"

/*
Copyright (c) 2021-2023 Devine Lu Linvega, Andrew Alderwick
Copyright (c) 2021 Adrian "asie" Siekierka
Copyright (c) 2021 Bad Diode
Copyright (c) 2023 Dave VanEe

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

#define FAST_BLIT 1 // Fast blit is 40-60% faster, based on simple testing

UxnScreen uxn_screen;

#if FAST_BLIT == 1
#define ROW_SPAN 64

static Uint64 dec_byte[256] = {
    0x0000000000000000, 0x0100000000000000, 0x0001000000000000, 0x0101000000000000, 0x0000010000000000,
    0x0100010000000000, 0x0001010000000000, 0x0101010000000000, 0x0000000100000000, 0x0100000100000000,
    0x0001000100000000, 0x0101000100000000, 0x0000010100000000, 0x0100010100000000, 0x0001010100000000,
    0x0101010100000000, 0x0000000001000000, 0x0100000001000000, 0x0001000001000000, 0x0101000001000000,
    0x0000010001000000, 0x0100010001000000, 0x0001010001000000, 0x0101010001000000, 0x0000000101000000,
    0x0100000101000000, 0x0001000101000000, 0x0101000101000000, 0x0000010101000000, 0x0100010101000000,
    0x0001010101000000, 0x0101010101000000, 0x0000000000010000, 0x0100000000010000, 0x0001000000010000,
    0x0101000000010000, 0x0000010000010000, 0x0100010000010000, 0x0001010000010000, 0x0101010000010000,
    0x0000000100010000, 0x0100000100010000, 0x0001000100010000, 0x0101000100010000, 0x0000010100010000,
    0x0100010100010000, 0x0001010100010000, 0x0101010100010000, 0x0000000001010000, 0x0100000001010000,
    0x0001000001010000, 0x0101000001010000, 0x0000010001010000, 0x0100010001010000, 0x0001010001010000,
    0x0101010001010000, 0x0000000101010000, 0x0100000101010000, 0x0001000101010000, 0x0101000101010000,
    0x0000010101010000, 0x0100010101010000, 0x0001010101010000, 0x0101010101010000, 0x0000000000000100,
    0x0100000000000100, 0x0001000000000100, 0x0101000000000100, 0x0000010000000100, 0x0100010000000100,
    0x0001010000000100, 0x0101010000000100, 0x0000000100000100, 0x0100000100000100, 0x0001000100000100,
    0x0101000100000100, 0x0000010100000100, 0x0100010100000100, 0x0001010100000100, 0x0101010100000100,
    0x0000000001000100, 0x0100000001000100, 0x0001000001000100, 0x0101000001000100, 0x0000010001000100,
    0x0100010001000100, 0x0001010001000100, 0x0101010001000100, 0x0000000101000100, 0x0100000101000100,
    0x0001000101000100, 0x0101000101000100, 0x0000010101000100, 0x0100010101000100, 0x0001010101000100,
    0x0101010101000100, 0x0000000000010100, 0x0100000000010100, 0x0001000000010100, 0x0101000000010100,
    0x0000010000010100, 0x0100010000010100, 0x0001010000010100, 0x0101010000010100, 0x0000000100010100,
    0x0100000100010100, 0x0001000100010100, 0x0101000100010100, 0x0000010100010100, 0x0100010100010100,
    0x0001010100010100, 0x0101010100010100, 0x0000000001010100, 0x0100000001010100, 0x0001000001010100,
    0x0101000001010100, 0x0000010001010100, 0x0100010001010100, 0x0001010001010100, 0x0101010001010100,
    0x0000000101010100, 0x0100000101010100, 0x0001000101010100, 0x0101000101010100, 0x0000010101010100,
    0x0100010101010100, 0x0001010101010100, 0x0101010101010100, 0x0000000000000001, 0x0100000000000001,
    0x0001000000000001, 0x0101000000000001, 0x0000010000000001, 0x0100010000000001, 0x0001010000000001,
    0x0101010000000001, 0x0000000100000001, 0x0100000100000001, 0x0001000100000001, 0x0101000100000001,
    0x0000010100000001, 0x0100010100000001, 0x0001010100000001, 0x0101010100000001, 0x0000000001000001,
    0x0100000001000001, 0x0001000001000001, 0x0101000001000001, 0x0000010001000001, 0x0100010001000001,
    0x0001010001000001, 0x0101010001000001, 0x0000000101000001, 0x0100000101000001, 0x0001000101000001,
    0x0101000101000001, 0x0000010101000001, 0x0100010101000001, 0x0001010101000001, 0x0101010101000001,
    0x0000000000010001, 0x0100000000010001, 0x0001000000010001, 0x0101000000010001, 0x0000010000010001,
    0x0100010000010001, 0x0001010000010001, 0x0101010000010001, 0x0000000100010001, 0x0100000100010001,
    0x0001000100010001, 0x0101000100010001, 0x0000010100010001, 0x0100010100010001, 0x0001010100010001,
    0x0101010100010001, 0x0000000001010001, 0x0100000001010001, 0x0001000001010001, 0x0101000001010001,
    0x0000010001010001, 0x0100010001010001, 0x0001010001010001, 0x0101010001010001, 0x0000000101010001,
    0x0100000101010001, 0x0001000101010001, 0x0101000101010001, 0x0000010101010001, 0x0100010101010001,
    0x0001010101010001, 0x0101010101010001, 0x0000000000000101, 0x0100000000000101, 0x0001000000000101,
    0x0101000000000101, 0x0000010000000101, 0x0100010000000101, 0x0001010000000101, 0x0101010000000101,
    0x0000000100000101, 0x0100000100000101, 0x0001000100000101, 0x0101000100000101, 0x0000010100000101,
    0x0100010100000101, 0x0001010100000101, 0x0101010100000101, 0x0000000001000101, 0x0100000001000101,
    0x0001000001000101, 0x0101000001000101, 0x0000010001000101, 0x0100010001000101, 0x0001010001000101,
    0x0101010001000101, 0x0000000101000101, 0x0100000101000101, 0x0001000101000101, 0x0101000101000101,
    0x0000010101000101, 0x0100010101000101, 0x0001010101000101, 0x0101010101000101, 0x0000000000010101,
    0x0100000000010101, 0x0001000000010101, 0x0101000000010101, 0x0000010000010101, 0x0100010000010101,
    0x0001010000010101, 0x0101010000010101, 0x0000000100010101, 0x0100000100010101, 0x0001000100010101,
    0x0101000100010101, 0x0000010100010101, 0x0100010100010101, 0x0001010100010101, 0x0101010100010101,
    0x0000000001010101, 0x0100000001010101, 0x0001000001010101, 0x0101000001010101, 0x0000010001010101,
    0x0100010001010101, 0x0001010001010101, 0x0101010001010101, 0x0000000101010101, 0x0100000101010101,
    0x0001000101010101, 0x0101000101010101, 0x0000010101010101, 0x0100010101010101, 0x0001010101010101,
    0x0101010101010101
};

static Uint64 dec_byte_flip_x[256] = {
    0x0000000000000000, 0x0000000000000001, 0x0000000000000100, 0x0000000000000101, 0x0000000000010000,
    0x0000000000010001, 0x0000000000010100, 0x0000000000010101, 0x0000000001000000, 0x0000000001000001,
    0x0000000001000100, 0x0000000001000101, 0x0000000001010000, 0x0000000001010001, 0x0000000001010100,
    0x0000000001010101, 0x0000000100000000, 0x0000000100000001, 0x0000000100000100, 0x0000000100000101,
    0x0000000100010000, 0x0000000100010001, 0x0000000100010100, 0x0000000100010101, 0x0000000101000000,
    0x0000000101000001, 0x0000000101000100, 0x0000000101000101, 0x0000000101010000, 0x0000000101010001,
    0x0000000101010100, 0x0000000101010101, 0x0000010000000000, 0x0000010000000001, 0x0000010000000100,
    0x0000010000000101, 0x0000010000010000, 0x0000010000010001, 0x0000010000010100, 0x0000010000010101,
    0x0000010001000000, 0x0000010001000001, 0x0000010001000100, 0x0000010001000101, 0x0000010001010000,
    0x0000010001010001, 0x0000010001010100, 0x0000010001010101, 0x0000010100000000, 0x0000010100000001,
    0x0000010100000100, 0x0000010100000101, 0x0000010100010000, 0x0000010100010001, 0x0000010100010100,
    0x0000010100010101, 0x0000010101000000, 0x0000010101000001, 0x0000010101000100, 0x0000010101000101,
    0x0000010101010000, 0x0000010101010001, 0x0000010101010100, 0x0000010101010101, 0x0001000000000000,
    0x0001000000000001, 0x0001000000000100, 0x0001000000000101, 0x0001000000010000, 0x0001000000010001,
    0x0001000000010100, 0x0001000000010101, 0x0001000001000000, 0x0001000001000001, 0x0001000001000100,
    0x0001000001000101, 0x0001000001010000, 0x0001000001010001, 0x0001000001010100, 0x0001000001010101,
    0x0001000100000000, 0x0001000100000001, 0x0001000100000100, 0x0001000100000101, 0x0001000100010000,
    0x0001000100010001, 0x0001000100010100, 0x0001000100010101, 0x0001000101000000, 0x0001000101000001,
    0x0001000101000100, 0x0001000101000101, 0x0001000101010000, 0x0001000101010001, 0x0001000101010100,
    0x0001000101010101, 0x0001010000000000, 0x0001010000000001, 0x0001010000000100, 0x0001010000000101,
    0x0001010000010000, 0x0001010000010001, 0x0001010000010100, 0x0001010000010101, 0x0001010001000000,
    0x0001010001000001, 0x0001010001000100, 0x0001010001000101, 0x0001010001010000, 0x0001010001010001,
    0x0001010001010100, 0x0001010001010101, 0x0001010100000000, 0x0001010100000001, 0x0001010100000100,
    0x0001010100000101, 0x0001010100010000, 0x0001010100010001, 0x0001010100010100, 0x0001010100010101,
    0x0001010101000000, 0x0001010101000001, 0x0001010101000100, 0x0001010101000101, 0x0001010101010000,
    0x0001010101010001, 0x0001010101010100, 0x0001010101010101, 0x0100000000000000, 0x0100000000000001,
    0x0100000000000100, 0x0100000000000101, 0x0100000000010000, 0x0100000000010001, 0x0100000000010100,
    0x0100000000010101, 0x0100000001000000, 0x0100000001000001, 0x0100000001000100, 0x0100000001000101,
    0x0100000001010000, 0x0100000001010001, 0x0100000001010100, 0x0100000001010101, 0x0100000100000000,
    0x0100000100000001, 0x0100000100000100, 0x0100000100000101, 0x0100000100010000, 0x0100000100010001,
    0x0100000100010100, 0x0100000100010101, 0x0100000101000000, 0x0100000101000001, 0x0100000101000100,
    0x0100000101000101, 0x0100000101010000, 0x0100000101010001, 0x0100000101010100, 0x0100000101010101,
    0x0100010000000000, 0x0100010000000001, 0x0100010000000100, 0x0100010000000101, 0x0100010000010000,
    0x0100010000010001, 0x0100010000010100, 0x0100010000010101, 0x0100010001000000, 0x0100010001000001,
    0x0100010001000100, 0x0100010001000101, 0x0100010001010000, 0x0100010001010001, 0x0100010001010100,
    0x0100010001010101, 0x0100010100000000, 0x0100010100000001, 0x0100010100000100, 0x0100010100000101,
    0x0100010100010000, 0x0100010100010001, 0x0100010100010100, 0x0100010100010101, 0x0100010101000000,
    0x0100010101000001, 0x0100010101000100, 0x0100010101000101, 0x0100010101010000, 0x0100010101010001,
    0x0100010101010100, 0x0100010101010101, 0x0101000000000000, 0x0101000000000001, 0x0101000000000100,
    0x0101000000000101, 0x0101000000010000, 0x0101000000010001, 0x0101000000010100, 0x0101000000010101,
    0x0101000001000000, 0x0101000001000001, 0x0101000001000100, 0x0101000001000101, 0x0101000001010000,
    0x0101000001010001, 0x0101000001010100, 0x0101000001010101, 0x0101000100000000, 0x0101000100000001,
    0x0101000100000100, 0x0101000100000101, 0x0101000100010000, 0x0101000100010001, 0x0101000100010100,
    0x0101000100010101, 0x0101000101000000, 0x0101000101000001, 0x0101000101000100, 0x0101000101000101,
    0x0101000101010000, 0x0101000101010001, 0x0101000101010100, 0x0101000101010101, 0x0101010000000000,
    0x0101010000000001, 0x0101010000000100, 0x0101010000000101, 0x0101010000010000, 0x0101010000010001,
    0x0101010000010100, 0x0101010000010101, 0x0101010001000000, 0x0101010001000001, 0x0101010001000100,
    0x0101010001000101, 0x0101010001010000, 0x0101010001010001, 0x0101010001010100, 0x0101010001010101,
    0x0101010100000000, 0x0101010100000001, 0x0101010100000100, 0x0101010100000101, 0x0101010100010000,
    0x0101010100010001, 0x0101010100010100, 0x0101010100010101, 0x0101010101000000, 0x0101010101000001,
    0x0101010101000100, 0x0101010101000101, 0x0101010101010000, 0x0101010101010001, 0x0101010101010100,
    0x0101010101010101
};
#endif

/* c = !ch ? (color % 5 ? color >> 2 : 0) : color % 4 + ch == 1 ? 0 : (ch - 2 + (color & 3)) % 3 + 1; */

static Uint8 blending[4][16] = {
    {0, 0, 0, 0, 1, 0, 1, 1, 2, 2, 0, 2, 3, 3, 3, 0},
    {0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3},
    {1, 2, 3, 1, 1, 2, 3, 1, 1, 2, 3, 1, 1, 2, 3, 1},
    {2, 3, 1, 2, 2, 3, 1, 2, 2, 3, 1, 2, 2, 3, 1, 2}};

static void
screen_fill(Uint8 *layer, int x1, int y1, int x2, int y2, int color)
{
	int x, y, width = uxn_screen.width, height = uxn_screen.height;
	int fillWidth = x2 > width ? width - x1 : x2 - x1;
	for(y = y1; y < y2 && y < height; y++)
		memset(&layer[x1 + y * 512], color, fillWidth);
}

static void
screen_blit(Uint8 *layer, Uint8 *ram, Uint16 addr, int x1, int y1, int color, int flipx, int flipy, int twobpp)
{
	int v, h, width = uxn_screen.width, height = uxn_screen.height, opaque = (color % 5);

	#if FAST_BLIT == 0
		for(v = 0; v < 8; v++) {
			Uint16 c = ram[(addr + v) & 0xffff] | (twobpp ? (ram[(addr + v + 8) & 0xffff] << 8) : 0);
			Uint16 y = y1 + (flipy ? 7 - v : v);
			Uint32 y_offset = y * 512;
			for(h = 7; h >= 0; --h, c >>= 1) {
				Uint8 ch = (c & 1) | ((c >> 7) & 2);
				if(opaque || ch) {
					Uint32 x = x1 + (flipx ? 7 - h : h);
					if(x < width && y < height)
						layer[x + y_offset] = blending[ch][color];
				}
			}
		}
	#else
		// This edge case handling works because the textures have huge amounts of 
		//  unused space surrounding the visible texture region.
		if(x1 > 0xfff8) { 
			x1 = (x1 + 8) & 0xffff; // offset x1 to be positive, then shift the texture pointer!
			layer -= 8;
		} else if(x1 >= 480) return;
		if(y1 > 0xfff8) { y1 = -(0x10000 - y1); } else if(y1 >= 272) return;

		int shift_left = (x1 % 8) * 8, shift_right = (8 - (x1 % 8)) * 8;

		Uint64 *dst = &layer[(x1 & 0xfff8) + y1 * 512];
		Uint64 *lut = flipx ? dec_byte_flip_x : dec_byte;
		if(twobpp) {
			if(color == 1) {
				Uint64 mask = 0xffffffffffffffff;
				if(!flipy) {
					for(v = 0; v < 8; v++, dst += ROW_SPAN) {
						if ((y1 + v) >= height) break;
						Uint8 ch1 = ram[(addr + v) & 0xffff];
						Uint8 ch2 = ram[((addr + v) + 8) & 0xffff];
						Uint64 c = lut[ch1] | (lut[ch2] << 1);
						if(shift_left) {
							dst[0] = (dst[0] & ~(mask << shift_left)) | (c << shift_left);
							dst[1] = (dst[1] & ~(mask >> shift_right)) | (c >> shift_right);
						} else dst[0] = c;
					}
				} else {
					for(v = 0; v < 8; v++, dst += ROW_SPAN) {
						if ((y1 + v) >= height) break;
						Uint8 ch1 = ram[(addr + (7 - v)) & 0xffff];
						Uint8 ch2 = ram[((addr + (7 - v)) + 8) & 0xffff];
						Uint64 c = lut[ch1] | (lut[ch2] << 1);
						if(shift_left) {
							dst[0] = (dst[0] & ~(mask << shift_left)) | (c << shift_left);
							dst[1] = (dst[1] & ~(mask >> shift_right)) | (c >> shift_right);
						} else dst[0] = c;
					}
				}
			} else if(opaque) {
				Uint64 mask = 0xffffffffffffffff;
				Uint8 c0 = blending[0][color];
				Uint8 c1 = blending[1][color];
				Uint8 c2 = blending[2][color];
				Uint8 c3 = blending[3][color];
				if(!flipy) {
					for(v = 0; v < 8; v++, dst += ROW_SPAN) {
						if ((y1 + v) >= height) break;
						Uint8 ch1 = ram[(addr + v) & 0xffff];
						Uint8 ch2 = ram[((addr + v) + 8) & 0xffff];
						Uint64 c = lut[ch1] | (lut[ch2] << 1);
						Uint64 c1mask = (c & 0x1111111111111111);
						Uint64 c2mask = (c & 0x2222222222222222) >> 1;
						Uint64 c3mask = (c1mask & c2mask) * 0xf;
						c1mask &= ~c3mask;
						c2mask &= ~c3mask;
						c3mask = (c & c3mask) & 0x1111111111111111;
						Uint64 c0mask = ~(c1mask | c2mask | c3mask) & 0x1111111111111111;
						c = (c0 * c0mask) |
							(c1 * c1mask) |
							(c2 * c2mask) |
							(c3 * c3mask);
						if(shift_left) {
							dst[0] = (dst[0] & ~(mask << shift_left)) | (c << shift_left);
							dst[1] = (dst[1] & ~(mask >> shift_right)) | (c >> shift_right);
						} else dst[0] = c;
					}
				} else {
					for(v = 0; v < 8; v++, dst += ROW_SPAN) {
						if ((y1 + v) >= height) break;
						Uint8 ch1 = ram[(addr + (7 - v)) & 0xffff];
						Uint8 ch2 = ram[((addr + (7 - v)) + 8) & 0xffff];
						Uint64 c = lut[ch1] | (lut[ch2] << 1);
						Uint64 c1mask = (c & 0x1111111111111111);
						Uint64 c2mask = (c & 0x2222222222222222) >> 1;
						Uint64 c3mask = (c1mask & c2mask) * 0xf;
						c1mask &= ~c3mask;
						c2mask &= ~c3mask;
						c3mask = (c & c3mask) & 0x1111111111111111;
						Uint64 c0mask = ~(c1mask | c2mask | c3mask) & 0x1111111111111111;
						c = (c0 * c0mask) |
							(c1 * c1mask) |
							(c2 * c2mask) |
							(c3 * c3mask);
						if(shift_left) {
							dst[0] = (dst[0] & ~(mask << shift_left)) | (c << shift_left);
							dst[1] = (dst[1] & ~(mask >> shift_right)) | (c >> shift_right);
						} else dst[0] = c;
					}
				}
			} else {
				Uint8 c1 = blending[1][color];
				Uint8 c2 = blending[2][color];
				Uint8 c3 = blending[3][color];
				if(!flipy) {
					for(v = 0; v < 8; v++, dst += ROW_SPAN) {
						if ((y1 + v) >= height) break;
						Uint8 ch1 = ram[(addr + v) & 0xffff];
						Uint8 ch2 = ram[((addr + v) + 8) & 0xffff];
						Uint64 c = lut[ch1] | (lut[ch2] << 1);
						Uint64 c1mask = (c & 0x1111111111111111);
						Uint64 c2mask = (c & 0x2222222222222222) >> 1;
						Uint64 c3mask = (c1mask & c2mask) * 0xf;
						c1mask &= ~c3mask;
						c2mask &= ~c3mask;
						c3mask = (c & c3mask) & 0x1111111111111111;
						Uint64 mask = (c1mask | c2mask | c3mask) * 0xf;
						c = (c1 * c1mask) |
							(c2 * c2mask) |
							(c3 * c3mask);
						if(shift_left) {
							dst[0] = (dst[0] & ~(mask << shift_left)) | (c << shift_left);
							dst[1] = (dst[1] & ~(mask >> shift_right)) | (c >> shift_right);
						} else dst[0] = (dst[0] & ~mask) | c;
					}
				} else {
					for(v = 0; v < 8; v++, dst += ROW_SPAN) {
						if ((y1 + v) >= height) break;
						Uint8 ch1 = ram[(addr + (7 - v)) & 0xffff];
						Uint8 ch2 = ram[((addr + (7 - v)) + 8) & 0xffff];
						Uint64 c = lut[ch1] | (lut[ch2] << 1);
						Uint64 c1mask = (c & 0x1111111111111111);
						Uint64 c2mask = (c & 0x2222222222222222) >> 1;
						Uint64 c3mask = (c1mask & c2mask) * 0xf;
						c1mask &= ~c3mask;
						c2mask &= ~c3mask;
						c3mask = (c & c3mask) & 0x1111111111111111;
						Uint64 mask = (c1mask | c2mask | c3mask) * 0xf;
						c = (c1 * c1mask) |
							(c2 * c2mask) |
							(c3 * c3mask);
						if(shift_left) {
							dst[0] = (dst[0] & ~(mask << shift_left)) | (c << shift_left);
							dst[1] = (dst[1] & ~(mask >> shift_right)) | (c >> shift_right);
						} else dst[0] = (dst[0] & ~mask) | c;
					}
				}
			}
		// 1bpp
		} else {
			if(opaque) {
				Uint64 mask = 0xffffffffffffffff;
				if(!flipy) {
					for(v = 0; v < 8; v++, dst += ROW_SPAN) {
						if ((y1 + v) >= height) break;
						Uint8 ch1 = ram[(addr + v) & 0xffff];
						Uint64 color_1 = lut[ch1];
						Uint64 color_2 = (color_1 ^ 0xffffffffffffffff) & 0x1111111111111111;
						Uint64 c = (color_1 * (color & 3)) | (color_2 * (color >> 2));
						if(shift_left) {
							dst[0] = (dst[0] & ~(mask << shift_left)) | (c << shift_left);
							dst[1] = (dst[1] & ~(mask >> shift_right)) | (c >> shift_right);
						} else dst[0] = c;
					}
				} else {
					for(v = 0; v < 8; v++, dst += ROW_SPAN) {
						if ((y1 + v) >= height) break;
						Uint8 ch1 = ram[(addr + (7 - v)) & 0xffff];
						Uint64 color_1 = lut[ch1];
						Uint64 color_2 = (color_1 ^ 0xffffffffffffffff) & 0x1111111111111111;
						Uint64 c = (color_1 * (color & 3)) | (color_2 * (color >> 2));
						if(shift_left) {
							dst[0] = (dst[0] & ~(mask << shift_left)) | (c << shift_left);
							dst[1] = (dst[1] & ~(mask >> shift_right)) | (c >> shift_right);
						} else dst[0] = c;
					}
				}
			} else {
				if(!flipy) {
					for(v = 0; v < 8; v++, dst += ROW_SPAN) {
						if ((y1 + v) >= height) break;
						Uint8 ch1 = ram[(addr + v) & 0xffff];
						Uint64 c = lut[ch1];
						Uint64 mask = c * 0xf;
						c *= color & 3;
						if(shift_left) {
							dst[0] = (dst[0] & ~(mask << shift_left)) | (c << shift_left);
							dst[1] = (dst[1] & ~(mask >> shift_right)) | (c >> shift_right);
						} else dst[0] = (dst[0] & ~mask) | c;
					}
				} else {
					for(v = 0; v < 8; v++, dst += ROW_SPAN) {
						if ((y1 + v) >= height) break;
						Uint8 ch1 = ram[(addr + (7 - v)) & 0xffff];
						Uint64 c = lut[ch1];
						Uint64 mask = c * 0xf;
						c *= color & 3;
						if(shift_left) {
							dst[0] = (dst[0] & ~(mask << shift_left)) | (c << shift_left);
							dst[1] = (dst[1] & ~(mask >> shift_right)) | (c >> shift_right);
						} else dst[0] = (dst[0] & ~mask) | c;
					}
				}
			}
		}
	#endif
}

void
screen_palette(Uint8 *addr, Uint16 *clutBG, Uint16 *clutFG)
{
	int i, shift;
	for(i = 0, shift = 4; i < 4; ++i, shift ^= 4) {
		Uint8
			r = (addr[0 + i / 2] >> shift) & 0xf,
			g = (addr[2 + i / 2] >> shift) & 0xf,
			b = (addr[4 + i / 2] >> shift) & 0xf;
		uxn_screen.palette[i] = 0xf000 | b << 8 | g << 4 | r;
	}
	for(int i = 0; i < 4; i++) {
		clutBG[i] = uxn_screen.palette[i & 3];
		if(i == 0) clutFG[i] == 0x0000;
		else clutFG[i] = uxn_screen.palette[i & 3];
	}
	uxn_screen.changed = 1;
}

static int
clamp(int val, int min, int max)
{
	return (val >= min) ? (val <= max) ? val : max : min;
}

void
screen_resize(Uint16 width, Uint16 height)
{
	Uint8 *bg, *fg;
	Uint32 *pixels;
	width = clamp(width, 8, 480);
	height = clamp(height, 8, 272);
	uxn_screen.width = width;
	uxn_screen.height = height;
	screen_fill(uxn_screen.bg, 0, 0, uxn_screen.width, uxn_screen.height, 0);
	screen_fill(uxn_screen.fg, 0, 0, uxn_screen.width, uxn_screen.height, 0);
}

Uint8
screen_dei(Uxn *u, Uint8 addr)
{
	switch(addr) {
	case 0x22: return uxn_screen.width >> 8;
	case 0x23: return uxn_screen.width;
	case 0x24: return uxn_screen.height >> 8;
	case 0x25: return uxn_screen.height;
	default: return u->dev[addr];
	}
}

void
screen_deo(Uint8 *ram, Uint8 *d, Uint8 port)
{
	switch(port) {
	case 0x3:
		screen_resize(PEEK2(d + 2), uxn_screen.height);
		break;
	case 0x5:
		screen_resize(uxn_screen.width, PEEK2(d + 4));
		break;
	case 0xe: {
		Uint8 ctrl = d[0xe];
		Uint8 color = ctrl & 0x3;
		Uint16 x = PEEK2(d + 0x8);
		Uint16 y = PEEK2(d + 0xa);
		Uint8 *layer = (ctrl & 0x40) ? uxn_screen.fg : uxn_screen.bg;
		/* fill mode */
		if(ctrl & 0x80) {
			Uint16 x2 = uxn_screen.width;
			Uint16 y2 = uxn_screen.height;
			if(ctrl & 0x10) x2 = x, x = 0;
			if(ctrl & 0x20) y2 = y, y = 0;
			screen_fill(layer, x, y, x2, y2, color);
			uxn_screen.changed = 1;
		}
		/* pixel mode */
		else {
			Uint16 width = uxn_screen.width;
			Uint16 height = uxn_screen.height;
			if(x < width && y < height)
				layer[x + y * 512] = color;
			uxn_screen.changed = 1;
			if(d[0x6] & 0x1) POKE2(d + 0x8, x + 1); /* auto x+1 */
			if(d[0x6] & 0x2) POKE2(d + 0xa, y + 1); /* auto y+1 */
		}
		break;
	}
	case 0xf: {
		Uint8 i;
		Uint8 ctrl = d[0xf];
		Uint8 move = d[0x6];
		Uint8 length = move >> 4;
		Uint8 twobpp = !!(ctrl & 0x80);
		Uint8 *layer = (ctrl & 0x40) ? uxn_screen.fg : uxn_screen.bg;
		Uint8 color = ctrl & 0xf;
		Uint16 x = PEEK2(d + 0x8), dx = (move & 0x1) << 3;
		Uint16 y = PEEK2(d + 0xa), dy = (move & 0x2) << 2;
		Uint16 addr = PEEK2(d + 0xc), addr_incr = (move & 0x4) << (1 + twobpp);
		int flipx = (ctrl & 0x10), fx = flipx ? -1 : 1;
		int flipy = (ctrl & 0x20), fy = flipy ? -1 : 1;
		Uint16 dyx = dy * fx, dxy = dx * fy;
		for(i = 0; i <= length; i++) {
			screen_blit(layer, ram, addr, x + dyx * i, y + dxy * i, color, flipx, flipy, twobpp);
			addr += addr_incr;
		}
		uxn_screen.changed = 1;
		if(move & 0x1) POKE2(d + 0x8, x + dx * fx); /* auto x+8 */
		if(move & 0x2) POKE2(d + 0xa, y + dy * fy); /* auto y+8 */
		if(move & 0x4) POKE2(d + 0xc, addr);   /* auto addr+length */
		break;
	}
	}
}
