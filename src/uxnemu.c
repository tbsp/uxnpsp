#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

#include <pspkernel.h>
#include <pspdisplay.h>
#include <pspaudiolib.h>
#include <pspdebug.h>
#include <psputils.h>
#include <psppower.h>
#include <pspctrl.h>
#include <pspge.h>
#include <pspgu.h>
#include <psputility.h>

#include "uxn.h"

#pragma GCC diagnostic push
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma clang diagnostic ignored "-Wtypedef-redefinition"
#include "devices/system.h"
#include "devices/screen.h"
#include "devices/audio.h"
#include "devices/file.h"
#include "devices/controller.h"
#include "devices/mouse.h"
#include "devices/datetime.h"
#if defined(_WIN32) && defined(_WIN32_WINNT) && _WIN32_WINNT > 0x0602
#include <processthreadsapi.h>
#elif defined(_WIN32)
#include <windows.h>
#include <string.h>
#endif
#ifndef __plan9__
#define USED(x) (void)(x)
#endif
#pragma GCC diagnostic pop
#pragma clang diagnostic pop

/*
Copyright (c) 2021-2023 Devine Lu Linvega, Andrew Alderwick

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

#define WIDTH 480
#define HEIGHT 272
#define PAD 0
#define TIMEOUT_MS 334
#define BENCH 0

typedef struct rect {
	Uint16 x, y;
	Uint16 w, h;
} rect;

void *draw_buffer, *disp_buffer, *depth_buffer;
Uint8 *fg_texture, *bg_texture;

static rect emu_frame, mouse;
Uint32 lastControllerButtons, lastMouseButtons;
Uint8 audio_done[4] = {0, 0, 0, 0};

Uint16 deo_mask[] = {0xff28, 0x0300, 0xc028, 0x8000, 0x8000, 0x8000, 0x8000, 0x0000, 0x0000, 0x0000, 0xa260, 0xa260, 0x0000, 0x0000, 0x0000, 0x0000};
Uint16 dei_mask[] = {0x0000, 0x0000, 0x003c, 0x0014, 0x0014, 0x0014, 0x0014, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x07ff, 0x0000, 0x0000, 0x0000};

static Uint16 __attribute__((aligned(16))) gu_clutBG[4];
static Uint16 __attribute__((aligned(16))) gu_clutFG[4];
static Uint32 __attribute__((aligned(16))) gu_list[262144];

struct Vertex {
	Uint16 u, v;
	short x, y, z;
};

struct Vertex __attribute__((aligned(16))) emu_verticies[16];
int num_verticies;

static void GuDraw(void);

int
debug(char *msg, int val0, int val1)
{
	FILE* file = fopen("debug.txt", "a");
	fprintf(file, "%s: %d, %d\n", msg, val0, val1);
	fclose(file);
	return 0;
}

/* PSP boilerplate */

PSP_MODULE_INFO("VarvaraPSP", 0, 1, 3);

PSP_MAIN_THREAD_ATTR(THREAD_ATTR_USER | THREAD_ATTR_VFPU);

/* Exit callback */
int exit_callback(int arg1, int arg2, void *common)
{
	sceKernelExitGame();
	return 0;
}

/* Callback thread */
int CallbackThread(SceSize args, void *argp)
{
	int cbid;
	cbid = sceKernelCreateCallback("Exit Callback", exit_callback, NULL);
	sceKernelRegisterExitCallback(cbid);
	sceKernelSleepThreadCB();
	return 0;
}

/* Sets up the callback thread and returns its thread id */
int SetupCallbacks(void)
{
	int thid = 0;
	thid = sceKernelCreateThread("update_thread", CallbackThread, 0x11, 0xFA0, 0, 0);
	if(thid >= 0) sceKernelStartThread(thid, 0, 0);
	return thid;
}

/* PSP VRAM Routines */

static Uint32 staticOffset = 0;

static Uint32 getMemorySize(Uint32 width, Uint32 height, Uint32 psm)
{
	switch (psm)
	{
		case GU_PSM_T4:
			return (width * height) >> 1;

		case GU_PSM_T8:
			return width * height;

		case GU_PSM_5650:
		case GU_PSM_5551:
		case GU_PSM_4444:
		case GU_PSM_T16:
			return 2 * width * height;

		case GU_PSM_8888:
		case GU_PSM_T32:
			return 4 * width * height;

		default:
			return 0;
	}
}

void* getStaticVramBuffer(Uint32 width, Uint32 height, Uint32 psm)
{
	Uint32 memSize = getMemorySize(width,height,psm);
	void* result = (void*)staticOffset;
	staticOffset += memSize;

	return result;
}

void* getStaticVramTexture(Uint32 width, Uint32 height, Uint32 psm)
{
	void* result = getStaticVramBuffer(width,height,psm);
	return (void*)(((Uint32)result) + ((Uint32)sceGeEdramGetAddr()));
}

/* PSP Utility dialog functions */

pspUtilityMsgDialogParams dialog;

static void ConfigureDialog(pspUtilityMsgDialogParams *dialog, size_t dialog_size)
{
    memset(dialog, 0, dialog_size);

    dialog->base.size = dialog_size;
    sceUtilityGetSystemParamInt(PSP_SYSTEMPARAM_ID_INT_LANGUAGE,
				&dialog->base.language); // Prompt language
    sceUtilityGetSystemParamInt(PSP_SYSTEMPARAM_ID_INT_UNKNOWN,
				&dialog->base.buttonSwap); // X/O button swap

    dialog->base.graphicsThread = 0x11;
    dialog->base.accessThread = 0x13;
    dialog->base.fontThread = 0x12;
    dialog->base.soundThread = 0x10;
}

static void ShowMessageDialog(const char *message, int enableYesno)
{
    ConfigureDialog(&dialog, sizeof(dialog));
    dialog.mode = PSP_UTILITY_MSGDIALOG_MODE_TEXT;
	dialog.options = PSP_UTILITY_MSGDIALOG_OPTION_TEXT;
	
	if(enableYesno)
		dialog.options |= PSP_UTILITY_MSGDIALOG_OPTION_YESNO_BUTTONS|PSP_UTILITY_MSGDIALOG_OPTION_DEFAULT_NO;		
	
    strcpy(dialog.message, message);

    sceUtilityMsgDialogInitStart(&dialog);

    for(;;) {
		GuDraw();
		switch(sceUtilityMsgDialogGetStatus()) {
		case 2:
			sceUtilityMsgDialogUpdate(1);
			break;
		case 3:
			sceUtilityMsgDialogShutdownStart();
			break;
		case 0:
			return;
	}

	sceDisplayWaitVblankStart();
	sceGuSwapBuffers();
    }
}

/* devices */

static int
clamp(int val, int min, int max)
{
	return (val >= min) ? (val <= max) ? val : max : min;
}

static Uint8
audio_dei(int instance, Uint8 *d, Uint8 port)
{
	switch(port) {
	case 0x4: return audio_get_vu(instance);
	case 0x2: POKE2(d + 0x2, audio_get_position(instance)); /* fall through */
	default: return d[port];
	}
}

static void
audio_deo(int instance, Uint8 *d, Uint8 port, Uxn *u)
{
	if(port == 0xf) {
		audio_start(instance, d, u);
	}
}

Uint8
emu_dei(Uxn *u, Uint8 addr)
{
	Uint8 p = addr & 0x0f, d = addr & 0xf0;
	switch(d) {
	case 0x20: return screen_dei(u, addr);
	case 0x30: return audio_dei(0, &u->dev[d], p);
	case 0x40: return audio_dei(1, &u->dev[d], p);
	case 0x50: return audio_dei(2, &u->dev[d], p);
	case 0x60: return audio_dei(3, &u->dev[d], p);
	case 0xc0: return datetime_dei(u, addr);
	}
	return u->dev[addr];
}

void
emu_deo(Uxn *u, Uint8 addr)
{
	Uint8 p = addr & 0x0f, d = addr & 0xf0;
	switch(d) {
	case 0x00:
		system_deo(u, &u->dev[d], p);
		if(p > 0x7 && p < 0xe)
			screen_palette(&u->dev[0x8], gu_clutBG, gu_clutFG);
		break;
	case 0x10: console_deo(&u->dev[d], p); break;
	case 0x20: screen_deo(u->ram, &u->dev[d], p); break;
	case 0x30: audio_deo(0, &u->dev[d], p, u); break;
	case 0x40: audio_deo(1, &u->dev[d], p, u); break;
	case 0x50: audio_deo(2, &u->dev[d], p, u); break;
	case 0x60: audio_deo(3, &u->dev[d], p, u); break;
	case 0xa0: file_deo(0, u->ram, &u->dev[d], p); break;
	case 0xb0: file_deo(1, u->ram, &u->dev[d], p); break;
	}
}

#pragma mark - Generics

void audio_out_callback(int instance, unsigned short* samples, unsigned int len)
{
	audio_render(instance, samples, len);
}

void
audio_finished_handler(int instance)
{
	audio_done[instance] = 1;
}

void audioOutCallback0(void *buf, unsigned int reqn, void *userdata) { audio_out_callback(0, buf, reqn); }
void audioOutCallback1(void *buf, unsigned int reqn, void *userdata) { audio_out_callback(1, buf, reqn); }
void audioOutCallback2(void *buf, unsigned int reqn, void *userdata) { audio_out_callback(2, buf, reqn); }
void audioOutCallback3(void *buf, unsigned int reqn, void *userdata) { audio_out_callback(3, buf, reqn); }

void 
GFX_init() {
	draw_buffer = getStaticVramBuffer(512, 272, GU_PSM_4444);
	disp_buffer = getStaticVramBuffer(512, 272, GU_PSM_4444);
	getStaticVramTexture(512, 8, GU_PSM_T8); // scratch memory to allow for texture overflow, just to be safe
	bg_texture = getStaticVramTexture(512, 512, GU_PSM_T8);
	fg_texture = getStaticVramTexture(512, 512, GU_PSM_T8);
	depth_buffer = getStaticVramBuffer(512, 272, GU_PSM_4444);

	/* Point the uxn_screen directly to the PSP textures to draw to */
	uxn_screen.fg = fg_texture;
	uxn_screen.bg = bg_texture;

	sceGuInit();
	sceGuStart(GU_DIRECT, gu_list);

	sceGuDrawBuffer(GU_PSM_4444, draw_buffer, 512);
	sceGuDispBuffer(480, 272, disp_buffer, 512);
	sceGuOffset(2048 - (480 / 2), 2048 - (272 / 2));
	sceGuViewport(2048, 2048, 480, 272);

	sceGuEnable(GU_SCISSOR_TEST);
	sceGuScissor(0, 0, 480, 272);

	sceGuEnable(GU_ALPHA_TEST);
	sceGuAlphaFunc(GU_GREATER, 0, 0xff);

	sceGuEnable(GU_TEXTURE_2D);
	sceGuShadeModel(GU_FLAT);

	sceGuDisable(GU_DEPTH_TEST);
	sceGuDisable(GU_CULL_FACE);

	sceGuClutMode(GU_PSM_4444, 0, 0x0f, 0);

	sceGuFinish();
	sceGuSync(0, 0);

	sceDisplayWaitVblankStart();
	sceGuDisplay(GU_TRUE);
}

static int
set_size(void)
{
	emu_frame.x = (480 - uxn_screen.width) / 2;
	emu_frame.y = (272 - uxn_screen.height) / 2;
	emu_frame.w = uxn_screen.width;
	emu_frame.h = uxn_screen.height;

	// Subdivide layer textures into 64-pixel wide slices, which should yield a notable performance gain during drawing
	num_verticies = 0;
	int j = 0;
	while(j < uxn_screen.width) {
		int sliceWidth = 64;
		if(j + sliceWidth > emu_frame.w) sliceWidth = emu_frame.w - j;
		emu_verticies[num_verticies].u = j;
		emu_verticies[num_verticies].v = 0;
		emu_verticies[num_verticies].x = emu_frame.x + j;
		emu_verticies[num_verticies].y = emu_frame.y;
		emu_verticies[num_verticies+1].u = j + sliceWidth;
		emu_verticies[num_verticies+1].v = emu_frame.h;
		emu_verticies[num_verticies+1].x = emu_frame.x + j + sliceWidth;
		emu_verticies[num_verticies+1].y = emu_frame.y + emu_frame.h;
		num_verticies += 2;
		j += sliceWidth;
	}

	uxn_screen.fullClears = 2;
	return 1;
}

static void
GuDraw() {
	sceGuStart(GU_DIRECT, gu_list);

	if(uxn_screen.fullClears > 0) {
		sceKernelDcacheWritebackInvalidateAll(); // Not sure, but this seems to be required on hardware to get proper clears?
		uxn_screen.fullClears--;
		sceGuClearColor(0xff000000);
		sceGuClear(GU_COLOR_BUFFER_BIT);
	}
	
	sceGuTexMode(GU_PSM_T8, 0, 0, GU_FALSE);
	sceGuTexFunc(GU_TFX_REPLACE, GU_TCC_RGBA);
	sceGuTexEnvColor(0x0);
	sceGuTexWrap(GU_REPEAT, GU_REPEAT);
	sceGuTexFilter(GU_NEAREST, GU_NEAREST);

	sceGuClutLoad(1, gu_clutBG);
	sceGuTexImage(0, 512, 512, 512, bg_texture);
	sceGuDrawArray(GU_SPRITES, GU_TEXTURE_16BIT | GU_VERTEX_16BIT | GU_TRANSFORM_2D, num_verticies, NULL, emu_verticies);
	sceGuClutLoad(1, gu_clutFG);
	sceGuTexImage(0, 512, 512, 512, fg_texture);
	sceGuDrawArray(GU_SPRITES, GU_TEXTURE_16BIT | GU_VERTEX_16BIT | GU_TRANSFORM_2D, num_verticies, NULL, emu_verticies);

	sceKernelDcacheWritebackInvalidateAll();
	sceGuFinish();
	sceGuSync(0, 0);
}

static void
redraw(void)
{
	if(emu_frame.w != uxn_screen.width || emu_frame.h != uxn_screen.height)
		set_size();
	uxn_screen.changed = 0;

	GuDraw();

	sceDisplayWaitVblankStart();
	sceGuSwapBuffers();
}

static int
init()
{
	SetupCallbacks();
	GFX_init();
	pspAudioInit();
	pspAudioSetVolume(0, 0x8000, 0x8000);
	pspAudioSetVolume(1, 0x8000, 0x8000);
	pspAudioSetVolume(2, 0x8000, 0x8000);
	pspAudioSetVolume(3, 0x8000, 0x8000);
	pspAudioSetChannelCallback(0, audioOutCallback0, NULL);
	pspAudioSetChannelCallback(1, audioOutCallback1, NULL);
	pspAudioSetChannelCallback(2, audioOutCallback2, NULL);
	pspAudioSetChannelCallback(3, audioOutCallback3, NULL);
	sceCtrlSetSamplingCycle(0);
	sceCtrlSetSamplingMode(PSP_CTRL_MODE_ANALOG);
	//scePowerSetCpuClockFrequency(333);
	return 1;
}

int
promptForRestart() {
	SceCtrlData pad;

	ShowMessageDialog("Restart Varvara?", 1);
	uxn_screen.fullClears = 2;
	if(dialog.buttonPressed == PSP_UTILITY_MSGDIALOG_RESULT_YES)
		return 1;
	uxn_screen.changed = 1; // force redraw
	return 0;
}


int
system_error(const char *err)
{
	ShowMessageDialog(err, 0);
	return 0;
}

#pragma mark - Devices

/* Boot */

static int
start(Uxn *u, char *rom, int queue)
{
	free(u->ram);
	if(!uxn_boot(u, (Uint8 *)calloc(0x10000 * RAM_PAGES, sizeof(Uint8))))
		return system_error("Boot: Failed to start uxn.");
	if(!system_load(u, rom))
		return system_error("Boot: Failed to load launcher.rom.");
	u->dev[0x17] = queue;
	if(!uxn_eval(u, PAGE_PROGRAM))
		return system_error("Boot: Failed to eval rom.");
	return 1;
}

static void
restart(Uxn *u)
{
	screen_resize(WIDTH, HEIGHT);
	start(u, "launcher.rom", 0);
}

static Uint8
get_controller_buttons_state(Uint32 buttons) {
	Uint8 state = 0;
	if(buttons & PSP_CTRL_CIRCLE) state |= 0x01;
	if(buttons & PSP_CTRL_CROSS) state |= 0x02;
	if(buttons & PSP_CTRL_SELECT) state |= 0x04;
	if(buttons & PSP_CTRL_START) state |= 0x08;
	if(buttons & PSP_CTRL_UP) state |= 0x10;
	if(buttons & PSP_CTRL_DOWN) state |= 0x20;
	if(buttons & PSP_CTRL_LEFT) state |= 0x40;
	if(buttons & PSP_CTRL_RIGHT) state |= 0x80;
	return state;
}

static Uint8
get_mouse_buttons_state(Uint32 buttons) {
	Uint8 state = 0;
	if(buttons & PSP_CTRL_RTRIGGER) state |= 0x01;
	if(buttons & PSP_CTRL_LTRIGGER) state |= 0x04;
	return state;
}

static int
handle_events(Uxn *u)
{
	SceCtrlData pad;
	sceCtrlReadBufferPositive(&pad, 1);
	if((pad.Buttons & PSP_CTRL_TRIANGLE) && promptForRestart())
		restart(u);
	/* Controller */
	else if((pad.Buttons & 24825) != lastControllerButtons) {
		controller_changed(u, &u->dev[0x80], get_controller_buttons_state(pad.Buttons));
		lastControllerButtons = pad.Buttons & 24825;
	}
	/* Mouse */
	Sint8 dx = (pad.Lx < 140 && pad.Lx > 116) ? 0 : (Sint8)(pad.Lx >> 3) - 16, dy = (pad.Ly < 140 && pad.Ly > 116) ? 0 : (Sint8)(pad.Ly >> 3) - 16;
	if(dx || dy) {
		mouse.x += dx;
		mouse.y += dy;
		mouse.x = mouse.x > 0x1000 ? 0 : mouse.x > uxn_screen.width ? uxn_screen.width : mouse.x;
		mouse.y = mouse.y > 0x1000 ? 0 : mouse.y > uxn_screen.height ? uxn_screen.height : mouse.y;
		mouse_pos(u, &u->dev[0x90], mouse.x, mouse.y);
	}
	if((pad.Buttons & 768) != lastMouseButtons) {
		mouse_changed(u, &u->dev[0x90], get_mouse_buttons_state(pad.Buttons));
		lastMouseButtons = pad.Buttons & 768;
	}
	/* Audio */
	for(int i = 0; i < 4; i++)
		if(audio_done[i]) { 
			audio_done[i] = 0;
			uxn_eval(u, PEEK2(&u->dev[0x30 + 0x10 * i]));
		}

	return 1;
}

static int
run(Uxn *u)
{
	uxn_screen.changed = 1; // force redraw
	for(;;) {
		Uint16 screen_vector;
		if(u->dev[0x0f])
			return system_error("Run: Ended.");
		if(!handle_events(u))
			return 0;
		screen_vector = PEEK2(&u->dev[0x20]);
		uxn_eval(u, screen_vector);
		if(uxn_screen.changed || uxn_screen.fullClears)
			redraw();
		else sceDisplayWaitVblankStart();
	}
}

int
main(int argc, char **argv)
{
	Uxn u = {0};
	int i = 1;
	if(!init())
		return system_error("Init: Failed to initialize emulator.");
	screen_resize(WIDTH, HEIGHT);
	if(!start(&u, "launcher.rom", argc - i))
		return 0;
	run(&u);
	return 0;
}
